class Reminder < ApplicationRecord
  before_create :create_reminder_time

  validates :name, presence: true
  validates :phone_number, presence: true, length: { in: 10..11 }, numericality: { only_integer: true }

 def create_reminder_time
   # create a reminder time
   ## Check for last reminder by the same creator
   past_reminder = Reminder.where(phone_number: self.phone_number)

   if (past_reminder.empty?)
     ## If empty response create a reminder
     today = Date.today()
     if today.monday?
       self.next_reminder = today.next_day(6)
     else
       self.next_reminder = today.next_day(8 - today.wday)
     end
   else
     ## Else use old date to and create a reminder for the net available monday
     self.next_reminder = next_available_monday(past_reminder.last.next_reminder)
   end


   # Put into a Task for Heroku to run
   # Check each user if they a reminder that needs to go
   # send the reminder

   # Put into a Task for Heroku to run
   #
 end
  private

  def next_available_monday(last_scheduled_day)
    last_scheduled_day.next_day.next_day(6)
  end

end
