# frozen_string_literal: true

Rails.application.routes.draw do
  # resources :reminders
  root 'reminders#new'
  post 'reminders', to: 'reminders#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
