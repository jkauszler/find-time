class CreateReminders < ActiveRecord::Migration[6.0]
  def change
    create_table :reminders do |t|
      t.string :name
      t.bigint :phone_number
      t.text :notes

      t.timestamps
    end
  end
end
