class AddNextReminderToReminders < ActiveRecord::Migration[6.0]
  def change
    add_column :reminders, :next_reminder, :datetime, null: false
  end
end
